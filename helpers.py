from urllib.parse import urlparse
import requests
import gdal 
import ogr
import osr
import tarfile
from qgis.core import *
import os

def my_read_method(uri):
    
    parsed = urlparse(uri)
    if parsed.scheme.startswith('http'):
        return requests.get(uri).text
    else:
        return STAC_IO.default_read_text_method(uri)

def enclosure2vsi(enclosure, username=None, api_key=None):
    
    enclosure = enclosure

    parsed_url = urlparse(enclosure)
    
    if username is None:
        url = '/vsicurl/%s://%s%s' % (list(parsed_url)[0], list(parsed_url)[1], list(parsed_url)[2])
    else:
        url = '/vsicurl/%s://%s:%s@%s%s' % (list(parsed_url)[0], username, api_key, list(parsed_url)[1], list(parsed_url)[2])
    
    return url

def get_vector_epsg(layer):
    
    source_ds = ogr.Open(layer)
        
    epsg_code = source_ds.GetLayer().GetSpatialRef().GetAttrValue("AUTHORITY", 1)

    return int(epsg_code)


def get_raster_epsg(layer):
    
    source_ds = gdal.Open(layer)
        
    source = osr.SpatialReference()
    
    source.ImportFromWkt(source_ds.GetProjection())
    epsg_code = source.GetAttrValue("AUTHORITY", 1)
    
    return int(epsg_code)


def make_tarfile(output_filename, tar_files):
    
    if os.path.exists(output_filename):
    
        os.remove(output_filename)
    
    with tarfile.open(output_filename, "w:gz") as tar:
        
        for file in tar_files:
            tar.add(file)

            
def row_to_layer(row, username=None, api_key=None):

    print(row.title)
    gpkg_file = '{}.gpkg'.format(row['identifier'])
    
    vsi_url = enclosure2vsi(row.enclosure, username, api_key).replace('.tiff', '.rgb.tiff')

    if row.mediatype in ['image/tiff; application=geotiff']:   
    
        ds = gdal.Open(vsi_url)

        if (ds.RasterCount == 1):
            
            ds = None
            
            del(ds)
            
            return None, None

        else:
            gdal.Translate(gpkg_file, 
                           ds,
                           format='GPKG', 
                           creationOptions=['RASTER_TABLE={}'.format(row['identifier'])])

            ds = None
            
            del(ds)

        layer = QgsRasterLayer(gpkg_file, row.title)

        crs = layer.crs()

        crs.createFromId(get_raster_epsg(gpkg_file))

        layer.setCrs(crs)
        
    elif row.mediatype in ['application/geo+json']:
        
        gpkg_file = '{}.gpkg'.format(row['identifier'])
              
        vsi_url = enclosure2vsi(row.enclosure, username, api_key)

        ds = gdal.OpenEx(vsi_url)

        gdal.VectorTranslate(gpkg_file,
                             ds,
                             format='GPKG')

        layer = QgsVectorLayer(gpkg_file, row.title)

        crs = layer.crs()
        
        crs.createFromId(get_vector_epsg(gpkg_file))
        
        layer.setCrs(crs)

        mySymbol1 = QgsFillSymbol.createSimple({'color':'255,0,0',
                                                    'color_border':'255,0,0',
                                                    'width_border':'0.1',
                                                    'style':'solid'})

        myRenderer = layer.renderer()
        myRenderer.setSymbol(mySymbol1)
        layer.triggerRepaint()

    return gpkg_file, layer